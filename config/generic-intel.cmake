set(CMAKE_C_COMPILER icc CACHE STRING "C Compiler")
set(CMAKE_Fortran_COMPILER ifort CACHE STRING "Fortran Compiler")
set(CMAKE_Fortran_FLAGS_RELEASE "-O3 -DNDEBUG" CACHE STRING "Fortran flags for Release")
set(CMAKE_Fortran_FLAGS_DEBUG   "-g -traceback -check all" CACHE STRING "Fortran flags for Debug")
