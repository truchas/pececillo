set(CMAKE_C_COMPILER gcc CACHE STRING "C Compiler")
set(CMAKE_Fortran_COMPILER nagfor CACHE STRING "Fortran Compiler")
set(CMAKE_Fortran_FLAGS_RELEASE "-O3 -DNDEBUG -u -w=uda" CACHE STRING "Fortran flags for Release")
set(CMAKE_Fortran_FLAGS_DEBUG   "-g90 -g -gline -O0 -w=uda -C=all" CACHE STRING "Fortran flags for Debug")
