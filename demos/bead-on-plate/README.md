Laser Bead on Plate Demo Problem
--------------------------------

This demo problem simulates a simple laser weld process where a high-powered
laser scans a line across the surface of a metal plate.  A liquid melt pool
forms under the laser spot and travels with the laser motion.  Heat conduction
and phase change are modeled in the simulation.  This problem is also strongly
similar to laser powder bed additive manufacturing processes.

The domain is a brick approximately 7.4mm by 10.2mm by 3.4mm thick.
Material properties approximating those of 304 stainless steel are used.
The laser is modeled as a heat flux BC with Gaussian profile, which moves
at approximately 17 mm/s.  The length of the laser scan is 5mm.

There are two input files input-63.json and input-21.json. The only difference
is the mesh used: 63 micron resolution versus 21 micron.  The coarser problem
is long running, under an hour, and the finer problem very long running, a few
hours (serial, current Intel Xeon processors).  Run times can be shortened by
not running the simulation out so far in time.
