add_subdirectory(petaca)  # the petaca library
add_subdirectory(lib)     # core pececillo library
add_subdirectory(vof)     # pececillo vof advection library
add_subdirectory(flow)    # pececillo flow library and main program
add_subdirectory(hc)      # pececillo heat conduction library and main program
add_subdirectory(pc)      # pececillo phase change library and main program
