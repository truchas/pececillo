set(SRC
    NS_solver_type.F90
    flow_sim_type.F90
    differential_operators_module.F90
    matl_props_type.F90
    prescribed_velocity_fields.F90
    velocity_to_faces_func.F90
    body_force_module.F90
    predictor_module.F90
    fischer_guess_type.F90
    projection_module.F90
)

add_library(pec_flow ${SRC})
add_library(pececillo::flow ALIAS pec_flow)

set(LIB_MOD_DIR ${CMAKE_CURRENT_BINARY_DIR}/mod_files/)
set_target_properties(pec_flow PROPERTIES Fortran_MODULE_DIRECTORY ${LIB_MOD_DIR})
target_link_libraries(pec_flow pececillo::vof pececillo::core petaca)

target_include_directories(pec_flow PUBLIC
    $<BUILD_INTERFACE:${LIB_MOD_DIR}>
    $<INSTALL_INTERFACE:include>
)

install(TARGETS pec_flow
        EXPORT  pec_flow
        LIBRARY DESTINATION lib
        ARCHIVE DESTINATION lib
)
install(DIRECTORY ${LIB_MOD_DIR} DESTINATION include)

# Add Support for OpenMP.  This needs to be present in the CMakeFile
# that controls linking of the final executable.
# find_package(OpenMP)
# if (OPENMP_FOUND)
#     set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
#     set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
#     set (CMAKE_Fortran_FLAGS "${CMAKE_Fortran_FLAGS} ${OpenMP_C_FLAGS}")
# endif()

#if(ENABLE_TESTS)
#  add_subdirectory(test)
#endif()

# Flow solver program
add_executable(pececillo-flow flow_main.F90)
target_link_libraries(pececillo-flow pececillo::flow)
install(TARGETS pececillo-flow DESTINATION bin)
