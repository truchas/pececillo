set(SRC
    PC_model_type.F90
    PC_precon_type.F90
    PC_AE_solver_type.F90
    PC_norm_type.F90
    PC_idaesol_model_type.F90
    PC_solver_type.F90
    PC_sim_type.F90
    material_type.F90
)

add_library(pec_pc ${SRC})
add_library(pececillo::pc ALIAS pec_pc)

set(LIB_MOD_DIR ${CMAKE_CURRENT_BINARY_DIR}/mod_files/)
set_target_properties(pec_pc PROPERTIES Fortran_MODULE_DIRECTORY ${LIB_MOD_DIR})

target_link_libraries(pec_pc pececillo::core petaca)

target_include_directories(pec_pc PUBLIC
    $<BUILD_INTERFACE:${LIB_MOD_DIR}>
    $<INSTALL_INTERFACE:include>
)

install(TARGETS pec_pc
        EXPORT  pec_pc
        LIBRARY DESTINATION lib
        ARCHIVE DESTINATION lib
)
install(DIRECTORY ${LIB_MOD_DIR} DESTINATION include)

# # Add Support for OpenMP.  This needs to be present in the CMakeFile
# # that controls linking of the final executable.
# find_package(OpenMP)
# if (OPENMP_FOUND)
#     set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
#     set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
#     set (CMAKE_Fortran_FLAGS "${CMAKE_Fortran_FLAGS} ${OpenMP_C_FLAGS}")
# endif()

if(ENABLE_TESTS)
  add_subdirectory(test)
endif()

# Phase change solver program
add_executable(pececillo-pc PC_main.F90)
target_link_libraries(pececillo-pc pececillo::pc)
install(TARGETS pececillo-pc DESTINATION bin)
