program test_material_type

  use,intrinsic :: iso_fortran_env, only: r8 => real64
#ifdef NAGFOR
  use,intrinsic :: f90_unix, only: exit
#endif
  use material_type
  use parameter_list_type
  implicit none
  
  integer :: status = 0

  call example

  call exit(status)

contains

  subroutine example
    type(material), target :: mat
    type(parameter_list) :: params
    real(r8) :: T, dT, H, dH
    
    call params%set('density', 1.0_r8)
    call params%set('specific-heat', 1.0_r8)
    call params%set('solidus-temp', 2.0_r8)
    call params%set('liquidus-temp', 3.0_r8)
    call params%set('latent-heat', 5.0_r8)
    call params%set('smoothing-radius', 0.45_r8)

    call mat%init(params)

!    dT = 0.125_r8
!    T = 0.0_r8
!    do while (T <= 5.0_r8)
!      print *, T, mat%enthalpy_density(T), mat%enthalpy_density_deriv(T)
!      T = T + dT
!    end do

!    dH = 0.125_r8
!    H = 0.0_r8
!    do while (H <= 10.0_r8)
!      T = mat%temp(H)
!      print *, H, T, mat%enthalpy_density(T)
!      H = H + dH
!    end do

    dT = 0.125_r8
    T = dT
    do while (T <= 5.0_r8)
      H = mat%enthalpy_density(T)
      print *, T, H, abs((mat%temp(H)-T)/T)
      T = T + dT
    end do
    
    call mat%dump_stats
  
  end subroutine example

end program
