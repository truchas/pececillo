!!
!! GEOMETRIC_ADVECTOR_TYPE
!!
!! This module defines a class that encapsulates a generic advector
!! for scalar fields
!!
!! Zechariah J. Jibben <zjibben@lanl.gov>
!! May 2017
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
!! This file is part of Truchas. 3-Clause BSD license; see the LICENSE file.
!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

module geometric_advector_type

  use kinds, only: r8
  use unstr_mesh_type
  use mesh_geom_type
  implicit none
  private

  type, public :: geometric_advector
    private

    type(unstr_mesh), pointer, public :: mesh  => null() ! reference only -- do not own
    type(mesh_geom),  pointer, public :: gmesh => null() ! reference only -- do not own
    real(r8), pointer :: volume_flux(:,:,:)    => null() ! reference only -- do not own
    real(r8), pointer :: fluxing_velocity(:,:) => null() ! reference only -- do not own
  contains
    procedure :: init
    procedure :: advect
    procedure :: advect_cell_delta
    procedure :: advect_bc_delta
  end type geometric_advector

contains

  subroutine init (this, volume_flux, fluxing_velocity, mesh, gmesh)

    class(geometric_advector), intent(out) :: this
    real(r8), target, intent(in) :: volume_flux(:,:,:), fluxing_velocity(:,:)
    type(unstr_mesh), target, intent(in)  :: mesh
    type(mesh_geom),  target, intent(in)  :: gmesh

    this%volume_flux => volume_flux
    this%fluxing_velocity => fluxing_velocity
    this%mesh => mesh
    this%gmesh => gmesh
    
  end subroutine init

  ! this routine will geometrically advect a given scalar field
  function advect (this, scalar_field, bc_data) result(scalar_delta)

    use bndry_func_class

    class(geometric_advector), intent(in) :: this
    real(r8), intent(in) :: scalar_field(:,:)
    class(bndry_func), intent(in) :: bc_data
    real(r8) :: scalar_delta(this%mesh%ncell)
    
    integer :: i,f,n, bndry_f, fid
    real(r8) :: flux, bc_value
    
    ! inflow boundary conditions
    scalar_delta = this%advect_bc_delta (bc_data)

    ! update through the rest of the mesh
    do i = 1,this%mesh%ncell
      scalar_delta(i) = scalar_delta(i) + this%advect_cell_delta (i, scalar_field)
    end do

  end function advect

  ! return the delta for advecting a scalar in a single cell
  ! neglects BCs. an associated call to advect_bc_delta is expected.
  function advect_cell_delta (this, i, scalar_field) result(scalar_delta)

    class(geometric_advector), intent(in) :: this
    integer, intent(in) :: i
    real(r8), intent(in) :: scalar_field(:,:)
    real(r8) :: scalar_delta

    integer :: f,n
    real(r8) :: flux

    scalar_delta = 0

    ! integrate flux over faces, neglecting inflow boundaries
    do f = 1,NFC
      ! upwind
      if (this%fluxing_velocity(f,i) < 0) then ! flowing out of cell
        n = this%gmesh%cneighbor(f,i)
        if (n > 0) then
          flux = sum(scalar_field(:,n) * this%volume_flux(:,f,i))
        else ! inflow boundaries handled seperately
          flux = 0
        end if
      else ! flowing into cell
        flux = sum(scalar_field(:,i) * this%volume_flux(:,f,i))
      end if

      scalar_delta = scalar_delta - flux
    end do

    scalar_delta = scalar_delta / this%mesh%volume(i)

  end function advect_cell_delta

  ! inflow boundary conditions
  function advect_bc_delta (this, bc_data) result(scalar_delta)

    use bndry_func_class

    class(geometric_advector), intent(in) :: this
    class(bndry_func), intent(in) :: bc_data
    real(r8) :: scalar_delta(this%mesh%ncell)
    
    integer :: i, fid, bndry_f
    real(r8) :: flux
    
    scalar_delta = 0
    do bndry_f = 1,size(bc_data%index)
      flux = bc_data%value(bndry_f)

      fid = bc_data%index(bndry_f)
      i = this%gmesh%fcell(1,fid) ! id of cell attached to this face

      scalar_delta(i) = scalar_delta(i) - flux / this%mesh%volume(i)
    end do

  end function advect_bc_delta

end module geometric_advector_type
